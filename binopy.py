'''
    Collimate binoculars using two webcams
    by
    Rupert Powell 2019
'''
import cv2
import numpy as np

capture = cv2.VideoCapture(1)
#capture1 = cv2.VideoCapture(2)
capture1 = capture
width = capture.get(3)   # float
height = capture.get(4) # float
print('Width={} Height={}'.format(width, height))
while True:
    _, frame1 = capture.read()
    _, frame2 = capture1.read()
    lineThickness = 1
    x1 = 0
    y1 = int(height / 2)
    x2 = int(width)
    y2 = int(y1)
    cv2.line(frame1, (x1, y1), (x2, y2), (0,255,0), lineThickness)
    cv2.line(frame2, (x1, y1), (x2, y2), (0,255,0), lineThickness)
    x1 = int(width / 2)
    y1 = int(0)
    x2 = int(x1)
    y2 = int(height)
    cv2.line(frame1, (x1, y1), (x2, y2), (0,255,0), lineThickness)
    cv2.line(frame2, (x1, y1), (x2, y2), (0,255,0), lineThickness)
    #frame2 = frame1
    cv2.imshow("Left",frame1)
    cv2.imshow("Right",frame2)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
capture.release()
capture1.release()
cv2.destroyAllWindows()